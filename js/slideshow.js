dhkSlide = {
    max:154,
    current:0,
    slide:function() {
      $("#imagelocation").fadeOut(1000,function() {dhkSlide.show();});
    },
    show:function() {
      $("#imagelocation").attr("src",dhkSlide.filename());
      $("#imagelocation").fadeIn(1000);
    },
    filename:function() {
      var rnd = Math.floor((Math.random() * this.max) + 1);
      while (rnd == dhkSlide.current) {
        rnd = Math.floor((Math.random() * this.max) + 1);
      } 
      dhkSlide.current = rnd;
      var pad = "000";
      var name = pad.substring(0, pad.length - (""+rnd).length) + rnd;
      return "./images/"+name+".jpg";
    }
}
setInterval(function(){dhkSlide.slide()},7000);